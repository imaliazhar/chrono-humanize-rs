macro_rules! duration_test {
    ($($name:ident: $duration:expr, $accuracy:expr, $tense:expr, $level: expr,)+) => {
        $(#[test]
        fn $name() {
            let ht = HumanTime::from($duration);
            let level = ht.to_text_en($accuracy, $tense);
            assert_eq!($level, level);
        })+
    }
}

#[cfg(test)]
mod duration {
    use chrono::Duration;
    use chrono_humanize::{Accuracy, HumanTime, Tense};

    // test_name: Duration expression, Accuray expression, Tense expression,"Level text"
    duration_test! {
        lvl0: Duration::weeks(50), Accuracy::Level(0), Tense::Present, "0 seconds",
        now_lvl2_present: Duration::zero(), Accuracy::Level(2), Tense::Present, "0 seconds",
        plus_1s_lvl1_past: Duration::seconds(1), Accuracy::Level(1), Tense::Past, "1 second ago",
        plus_1s_lvl1_future: Duration::seconds(1), Accuracy::Level(1), Tense::Future, "in 1 second",
        plus_95s_lvl1_present: Duration::seconds(95), Accuracy::Level(1), Tense::Present, "1 minute",
        minus_95s_lvl1_past: Duration::seconds(-95), Accuracy::Level(1), Tense::Past, "1 minute ago",
        plus_24w_lvl3_present: Duration::weeks(24), Accuracy::Level(3), Tense::Present, "5 months, 2 weeks and 4 days",
        minus_24w_lvl1_past: Duration::weeks(-24), Accuracy::Level(1), Tense::Past, "5 months ago",
        plus_50w_lvl5_present: Duration::weeks(50), Accuracy::Level(5), Tense::Present, "11 months, 2 weeks and 6 days",
        plus_101w_lvl4_future: Duration::weeks(101), Accuracy::Level(4), Tense::Future, "in 1 year, 11 months, 1 week and 5 days",
        minus_101w_lvl2_past: Duration::weeks(-101), Accuracy::Level(2), Tense::Past, "1 year and 11 months ago",
    }
}
